const UI = (function () {

    const ul = document.querySelector('.list-group');
    const emptyAlert = document.querySelector('.empty-alert');

    const listTemplate = function (task) {
        // Create list item
        let li = document.createElement('li');
        li.className = 'list-group-item d-flex align-item-center';
        li.setAttribute('data-id', task.id);

        // Create span
        let span = document.createElement('span');
        span.classList = 'taskName';
        span.textContent = task.text;

        // Create tag i fa-trash-alt
        let iDelete = document.createElement('i');
        iDelete.className = 'fas fa-trash-alt delete-item ml-4';

        // Create tag i fa-trash-alt
        let iEdit = document.createElement('i');
        iEdit.className = 'fas fa-edit edit-item ml-auto';

        // Append delete and edit icons to li
        li.appendChild(span);
        li.appendChild(iEdit);
        li.appendChild(iDelete);

        return li;
    };

    const addTask = function (task) {
        ul.insertAdjacentElement('afterbegin', listTemplate(task));
    };

    const deleteTask = function (id) {
        const li = ul.querySelector(`[data-id="${id}"]`);
        li.remove();
    };

    const checkList = function () {
        if (!ul.children.length) {
            emptyAlert.style.display = 'block';
        } else {
            emptyAlert.style.display = 'none';
        }
    };

    const deleteAll = async function () {
        ul.innerHTML = '';
    };

    const redactTask = async function (target) {
        let parent = target.closest('li');
        let itemId = parent.getAttribute('data-id');
        let item = parent.querySelector('.taskName');

        target.classList.toggle('fa-save');

        if (target.classList.contains('fa-save')) {
            item.setAttribute('contenteditable', 'true');
            item.focus();
        } else {
            item.setAttribute('contenteditable', 'false');
            item.blur();
        }

        return {
            id: itemId,
            text: item.textContent
        }
    };

    return {
        addTask,
        deleteTask,
        checkList,
        deleteAll,
        redactTask
    }
}());
