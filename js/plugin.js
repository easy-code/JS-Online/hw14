let tasks = JSON.parse(localStorage.getItem('tasks')) || [];

let ul = document.querySelector('.list-group'),
    form = document.forms['addTodoItem'],
    inputText = form.elements['todoText'],
    successAlert = document.querySelector('.alert-success'),
    removeAlert = document.querySelector('.alert-danger'),
    infoAlert = document.querySelector('.alert-info'),
    updateAlert = document.querySelector('.alert-secondary'),
    clearListBtn = document.querySelector('.clear-btn');

function generateId() {
    let id = '';
    let words = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';

    for (let i = 0; i < 15; i++) {
        let position = Math.floor(Math.random() * words.length);
        id += words[position];
    }

    return id;
}

function listTemplate(task) {
    // Create list item
    let li = document.createElement('li');
    li.className = 'list-group-item d-flex align-item-center';
    li.setAttribute('data-id', task.id);

    // Create span
    let span = document.createElement('span');
    span.textContent = task.text;

    // Create tag i fa-trash-alt
    let iDelete = document.createElement('i');
    iDelete.className = 'fas fa-trash-alt delete-item ml-4';

    // Create tag i fa-trash-alt
    let iEdit = document.createElement('i');
    iEdit.className = 'fas fa-edit edit-item ml-auto';

    // Append delete and edit icons to li
    li.appendChild(span);
    li.appendChild(iEdit);
    li.appendChild(iDelete);

    return li;
}

function clearList() {
    ul.innerHTML = '';
}

function generateList(tasksArray) {
    clearList();
    checkEmpty(tasks);

    for (let i = 0; i < tasksArray.length; i++) {
        let li = listTemplate(tasksArray[i]);
        ul.appendChild(li);
    }
}

function addList(list) {
    let newTask = {
        id: generateId(),
        text: list
    };

    tasks.unshift(newTask);
    ul.insertAdjacentElement('afterbegin', listTemplate(newTask));
    showMessage(successAlert);
    localStorage.setItem('tasks', JSON.stringify(tasks));
}

function deleteListItem(id) {
    for (let i = 0; i < tasks.length; i++) {
        if (tasks[i].id === id) {
            tasks.splice(i, 1);
            break;
        }
    }

    localStorage.setItem('tasks', JSON.stringify(tasks));
}

function editListItem(id, newValue) {
    for (let i = 0; i < tasks.length; i++) {
        if (tasks[i].id === id) {
            tasks[i].text = newValue;
            break;
        }
    }

    showMessage(updateAlert);
    localStorage.setItem('tasks', JSON.stringify(tasks));
}

ul.addEventListener('click', function (e) {
    let parent = e.target.closest('li'),
        id = parent.dataset.id;

    if (e.target.classList.contains('delete-item')) {
        showMessage(removeAlert);
        deleteListItem(id);
        parent.remove();
        checkEmpty(tasks);
    } else if (e.target.classList.contains('edit-item')) {
        e.target.classList.toggle('fa-save');
        let span = parent.querySelector('span');

        if (e.target.classList.contains('fa-save')) {
            span.setAttribute('contenteditable', 'true');
            span.focus();
        } else {
            span.setAttribute('contenteditable', 'false');
            span.blur();
            editListItem(id, span.textContent);
        }
    }
});

form.addEventListener('submit', function (e) {
    e.preventDefault();
    if (!inputText.value) {
        inputText.classList.add('is-invalid');
    } else {
        inputText.classList.remove('is-invalid');
        addList(inputText.value);
        form.reset();
        showMessage(successAlert);
        checkEmpty(tasks);
    }
});

inputText.addEventListener('keyup', function () {
    if (inputText.value) {
        inputText.classList.remove('is-invalid');
    }
});

// generate list
generateList(tasks);

function showMessage(alert) {
    alert.classList.add('d-block');

    setTimeout(function () {
        alert.classList.remove('d-block');
    }, 2000);
}

function checkEmpty(list) {
    infoAlert.style.marginBottom = '0px';
    list.length > 0 ? infoAlert.classList.add('d-none') : infoAlert.classList.remove('d-none');
}

clearListBtn.addEventListener('click', function () {
    tasks.splice(0, tasks.length);
    generateList(tasks);
});
